import 'package:flutter/material.dart';

Color primaryBlue = Color(0xff3080ED);
Color secondaryOrange = Color(0xffFF7801);
String openSans = "Open Sans";
String lato = "Lato";
String logoPath = "assets/login_logo.png";
Color outlineGrey = Colors.grey;
Color transparent = Colors.transparent;
Color white = Colors.white;
Color red = Colors.red;
const Color primary = const Color(0xff6E2A7F);
Color hyperlinkTextColor = const Color(0xff3776CC);
const Color secondary = const Color(0xffF2F2F3);
const Color secondaryGrey = const Color(0xffA9A9A9);
const Color secondaryBackground = const Color(0xffF6F6F6);
const Color blueLow = const Color (0xff5E2AF4);