import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

/// This function is used to convert the xd width to the device width
/// @param xdWidth is the width of the component in xd, @param deviceWidth is the width of the device
double convertXdWidthToScreen({required double xdWidth, required double deviceWidth, double xdScreenWidth = 411.0}) {
  return (xdWidth / xdScreenWidth) * deviceWidth;
}

/// This function is used to convert the xd height to the device height
/// @param xdHeight is the height of the component in xd, @param deviceHeight is the height of the device
double convertXdHeightToScreen({required double xdHeight, required double deviceHeight, double xdScreenHeight = 823.0}) {
  return (xdHeight / xdScreenHeight) * deviceHeight;
}
/// This function is used to launch a url in the browser or in the app "depending on platform"
/// @param url is the url to be launched
launchURL2(String uri) async {
  Uri _url = Uri.parse(uri);
  try {
    await launchUrl(_url,
        mode: LaunchMode.platformDefault
    );
  } catch (e) {
    print(e);
  }
}


/// This function is used to show a snack bar message to the user
/// @param context is the context of the screen
/// @param message is the message to be displayed
/// @param duration is the duration of the snack bar
/// @param color is the color of the snack bar
/// @param textColor is the color of the text of the snack bar
/// @param fontSize is the size of the text of the snack bar
/// @param fontWeight is the weight of the text of the snack bar
void showSnackBar({required BuildContext context, required String message, int duration = 4, Color color = Colors.red, Color textColor = Colors.white, double fontSize = 14, FontWeight fontWeight = FontWeight.w400}) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(message, style: TextStyle(color: textColor, fontSize: fontSize, fontWeight: fontWeight),),
      backgroundColor: color,
      duration: Duration(seconds: duration),
    ),
  );
}

double convertCelsiusToFahrenheit({required double celsius}) {
  double fahrenheit = (celsius * 9/5) + 32;
  return double.parse(fahrenheit.toStringAsFixed(2));
}

