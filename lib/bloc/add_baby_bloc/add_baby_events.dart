// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';
import 'package:image_picker/image_picker.dart';

import 'package:neoroo/models/infant_model.dart';
import 'package:neoroo/models/infant_mother.dart';

abstract class AddBabyEvents extends Equatable {}

class AddBabyEvent extends AddBabyEvents {
  final String birthDate;
  final String birthNotes;
  final String birthTime;
  final String birthWeight;
  final String bodyLength;
  final String cribNumber;
  final String neoDeviceID;
  final String headCircumference;
  final XFile? image;
  final String needResuscitation;
  final String wardNumber;
  final String presentWeight;
  final String motherName;
  final String motherId;
  final String stsTime;
  final String nstsTime;
  final String infantTemperature;
  final String infantHeartRate;
  final String infantRespirationRate;
  final String infantBloodOxygen;
  final String infantId;

  AddBabyEvent(
      this.birthDate,
      this.birthNotes,
      this.birthTime,
      this.birthWeight,
      this.bodyLength,
      this.cribNumber,
      this.neoDeviceID,
      this.headCircumference,
      this.image,
      this.needResuscitation,
      this.wardNumber,
      this.presentWeight,
      this.motherName,
      this.motherId,
      this.stsTime,
      this.nstsTime,
      this.infantTemperature,
      this.infantHeartRate,
      this.infantRespirationRate,
      this.infantBloodOxygen,
      this.infantId);
      
        @override
        // TODO: implement props
        List<Object?> get props => throw UnimplementedError();
}

class GetMotherEvent extends AddBabyEvents {
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();
}

class SearchInMotherList extends AddBabyEvents {
  final String motherName;
  final List<Mother> motherList;

  SearchInMotherList(
    this.motherName,
    this.motherList,
  );
  
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();
}

class SearchInfantFromECEBList extends AddBabyEvents {
  final String infantData;
  final List<Infant> ecebInfantSearchList;
  SearchInfantFromECEBList({
    required this.infantData,
    required this.ecebInfantSearchList,
  });
  
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();
}

class GetInfantsFromEceb extends AddBabyEvents {
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();
}

class EcebInfantSelected extends AddBabyEvents {
  final Infant infant;
  EcebInfantSelected({
    required this.infant,
  });
  
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();
}
