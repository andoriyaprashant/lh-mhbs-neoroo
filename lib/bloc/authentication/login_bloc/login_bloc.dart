import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:neoroo/bloc/authentication/login_bloc/login_bloc_events.dart';
import 'package:neoroo/bloc/authentication/login_bloc/login_bloc_states.dart';
import 'package:neoroo/repository/authentication_repository.dart';
import 'package:neoroo/repository/hive_storage_repository.dart';
import 'package:neoroo/exceptions/custom_exception.dart'; 

class LoginBloc extends Bloc<LoginEvents, LoginState> {
  final AuthenticationRepository authenticationRepository;
  final HiveStorageRepository hiveStorageRepository;

  // Constructor for the LoginBloc
  LoginBloc(this.authenticationRepository, this.hiveStorageRepository)
      : super(LoginInitial()) {
    on<LoginEvent>(_handleLogin);
    on<LocalLoginEvent>(_handleLocalLogin);
    on<LogoutEvent>(_handleLogout);
  }

  /// Handles user login events
  Future<void> _handleLogin(LoginEvent event, Emitter<LoginState> emit) async {
    emit(LoginLoading());

    try {
      final canAuthenticate = await authenticationRepository.isLocalAuthSupported();
      if (!canAuthenticate["status"]) {
        emit(LocalAuthSupportError(canAuthenticate["message"]));
        return;
      }

      final result = await authenticationRepository.loginUser(
        event.username,
        event.password,
        event.serverURL,
      );

      if (result is Map<String, dynamic>) {
        emit(LoginLoaded(
          orgUnits: result["orgUnits"],
          username: event.username,
        ));
      } else {
        emit(LoginGeneralError(result));
      }
    } catch (e) {
      emit(LoginGeneralError(
        CustomException(e.toString(), null),
      ));
    }
  }

  /// Handles local login events
  Future<void> _handleLocalLogin(LocalLoginEvent event, Emitter<LoginState> emit) async {
    emit(LoginLoading());

    try {
      final canAuthenticate = await authenticationRepository.isLocalAuthSupported();
      if (!canAuthenticate["status"]) {
        emit(LocalAuthSupportError(canAuthenticate["message"]));
        return;
      }

      final savedCredentials = await authenticationRepository.getSavedCredentials();
      if (!savedCredentials["status"]) {
        emit(LocalAuthSupportError(savedCredentials["message"]));
        return;
      }

      emit(LocalAuthSuccess(savedCredentials["data"]));
    } catch (e) {
      emit(LoginGeneralError(
        CustomException(e.toString(), null),
      ));
    }
  }

  /// Handles user logout events
  Future<void> _handleLogout(LogoutEvent event, Emitter<LoginState> emit) async {
    try {
      await hiveStorageRepository.logOutUser();
      emit(LoginInitial());
    } catch (e) {
      emit(LoginGeneralError(
        CustomException('Logout failed: ${e.toString()}', null),
      ));
    }
  }
}
