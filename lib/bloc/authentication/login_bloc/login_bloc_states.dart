import 'package:equatable/equatable.dart';
import 'package:neoroo/exceptions/custom_exception.dart';

// Abstract base class for login states
abstract class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object?> get props => [];
}

// Initial state when the login process starts
class LoginInitial extends LoginState {}

// Loading state during the login process
class LoginLoading extends LoginState {}

// State indicating successful login with organization units
class LoginLoaded extends LoginState {
  final List<String> orgUnits;
  final String username;

  const LoginLoaded({
    required this.orgUnits,
    required this.username,
  });

  @override
  List<Object?> get props => [orgUnits, username];
}

// State indicating a general error during login
class LoginGeneralError extends LoginState {
  final CustomException exception;

  const LoginGeneralError(this.exception);

  @override
  List<Object?> get props => [exception.message, exception.statusCode];
}

// State indicating an error in local authentication support
class LocalAuthSupportError extends LoginState {
  final String message;

  const LocalAuthSupportError(this.message);

  @override
  List<Object?> get props => [message];
}

// State indicating success in local authentication
class LocalAuthSuccess extends LoginState {
  final Map<String, List<String>> credentials;

  const LocalAuthSuccess(this.credentials);

  @override
  List<Object?> get props => [credentials];
}
