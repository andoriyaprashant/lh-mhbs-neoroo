// Abstract base class for login events
abstract class LoginEvents {
  const LoginEvents();
}

// Event for user login
class LoginEvent extends LoginEvents {
  final String serverURL;
  final String username;
  final String password;
  
  const LoginEvent(this.serverURL, this.password, this.username);
}

// Event for local login
class LocalLoginEvent extends LoginEvents {
  const LocalLoginEvent();
}

// Event for user logout
class LogoutEvent extends LoginEvents {
  const LogoutEvent();
}
