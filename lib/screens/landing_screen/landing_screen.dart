import 'dart:developer';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:neoroo/screens/choose_user_type/choose_user_type_screen.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../utils/vertical_space.dart';
import 'components/main_button.dart';
import 'components/multiline_with_hyperlink_text.dart';
import 'package:neoroo/routing/routing.dart' as router;
class LandingScreen extends StatefulWidget {
  const LandingScreen({Key? key}) : super(key: key);

  @override
  State<LandingScreen> createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> {
  late TapGestureRecognizer tapGestureRecognizer;
  @override
  void initState() {
    /// this is used to detect the tap gesture on the text, and perform an action on tap
    tapGestureRecognizer = TapGestureRecognizer();
    super.initState();
  }
  @override
  void dispose() {
    tapGestureRecognizer.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width;
    double deviceHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Center(

        child:
        // check if the device is in landscape mode or is a tablet
        // (deviceWidth > deviceHeight && deviceWidth > 600) || deviceWidth > 960 ?
        // Row(
        //   children: [
        //   Expanded(child:
        //   Image.asset("assets/images/logo.png", width: convertXdWidthToScreen(xdWidth: 169, deviceWidth: deviceWidth),),
        //   ),
        //   Expanded(child:
        //   Column(
        //     mainAxisAlignment: MainAxisAlignment.center,
        //     crossAxisAlignment: CrossAxisAlignment.center,
        //     children: [
        //       MainButton(deviceWidth: deviceWidth, deviceHeight: deviceHeight,
        //         title: "Get Started",
        //         buttonSize: Size(
        //         convertXdWidthToScreen(xdWidth: 308, deviceWidth: deviceWidth),
        //         convertXdHeightToScreen(xdHeight: 82, deviceHeight: deviceHeight),
        //       ),onPressed: () {
        //          Navigator.pushNamed(context, router.chooseUserTypePage);
        //       },
        //       ),
        //       VerticalSpace(height: convertXdHeightToScreen(xdHeight: 80, deviceHeight: deviceHeight),),
        //       MultiLineWithHyperlinkText(deviceWidth: deviceWidth, tapGestureRecognizer: tapGestureRecognizer,
        //       privacyPolicyLink: "https://www.google.com", cookiesPolicyLink: "https://www.google.com",
        //       ),
        //     ],
        //   ),
        //   )
        //   ],
        // ):
        Column(
          //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: (deviceWidth > deviceHeight && deviceWidth > 600)
              || deviceWidth > 960 ? MainAxisAlignment.spaceEvenly: MainAxisAlignment.center,
          children: [
            VerticalSpace(height: convertXdHeightToScreen(xdHeight: 110, deviceHeight: deviceHeight),),
            GestureDetector(onTap:(){
              log(deviceHeight.toString());
              log(deviceWidth.toString());
            },child: Image.asset("assets/images/logo.png",
              width:(deviceWidth > deviceHeight && deviceWidth > 600)
                  || deviceWidth > 960 ? null:
              convertXdWidthToScreen(xdWidth: 169, deviceWidth: deviceWidth),
            height: (deviceWidth > deviceHeight && deviceWidth > 600)
                || deviceWidth > 960 ? convertXdHeightToScreen(xdHeight: 169, deviceHeight: deviceWidth): null,
            )),

            VerticalSpace(height:
            (deviceWidth > deviceHeight && deviceWidth > 600)
                || deviceWidth > 960 ? 0:
            convertXdHeightToScreen(xdHeight: 160, deviceHeight: deviceHeight),),
            MainButton(deviceWidth: deviceWidth, deviceHeight: deviceHeight,
              title: "Get Started",
              buttonSize: Size(
              convertXdWidthToScreen(xdWidth: 308, deviceWidth: deviceWidth),
              convertXdHeightToScreen(xdHeight: 82, deviceHeight: deviceHeight),
            ),onPressed: () {
               Navigator.pushNamed(context, router.chooseUserTypePage);
            },
            ),
            VerticalSpace(height:
            (deviceWidth > deviceHeight && deviceWidth > 600)
                || deviceWidth > 960 ? 0:
            convertXdHeightToScreen(xdHeight: 80, deviceHeight: deviceHeight),),
            MultiLineWithHyperlinkText(deviceWidth: deviceWidth, tapGestureRecognizer: tapGestureRecognizer,
            privacyPolicyLink: "https://www.google.com", cookiesPolicyLink: "https://www.google.com",
            ),
          ],

        ),
      )

    );
  }
}


