import 'package:flutter/material.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/methods.dart';

/// This is a general button used in the Neoroo App, that can be customized with many parameters:
/// [deviceWidth] is the width of the device, used to convert the xd width to actual device width.
/// [deviceHeight] is the height of the device, used to convert the xd height to actual device height.
/// [title] is the text that will be displayed on the button.
/// [hasElevation] is a boolean that determines whether the button has elevation or not.
/// [textSize] is the size of the text on the button.
/// [textColor] is the color of the text on the button.
/// [buttonColor] is the color of the button.
/// [onPressed] is the function that will be called when the button is pressed.
/// [fontWeight] is the weight of the text on the button.
/// [buttonSize] is the size of the button.
/// [icon] is the icon that will be displayed on the button. it's an optional parameter.
class MainButton extends StatelessWidget {
  MainButton({
    super.key,
    required this.deviceWidth,
    required this.deviceHeight,
    required this.title,
    this.hasElevation=false,
    this.textSize=24,
    this.textColor=Colors.white,
    this.buttonColor=primary,
    required this.onPressed,
    this.fontWeight=FontWeight.w500,
    required this.buttonSize, this.icon,
    this.padding,
    this.rounded=true,
  });

  final double deviceWidth;
  final double deviceHeight;
  final String title;
  bool hasElevation;
  double textSize;
  Color textColor;
  Color buttonColor;
  FontWeight fontWeight;
  VoidCallback? onPressed;
  Size buttonSize;
  Widget? icon;
  EdgeInsets? padding;
  bool rounded;
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(onPressed: onPressed, child: icon!=null?
    Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(title,
          style: TextStyle(
              fontSize: textSize,
              fontWeight: fontWeight,
              color: textColor
          ),),
        icon!
      ],
    ):
    Text(title,
      style: TextStyle(
        fontSize: textSize,
        fontWeight: fontWeight,
        color: textColor
      ),),
      style: ButtonStyle(
        fixedSize: MaterialStateProperty.all(buttonSize),
        elevation: MaterialStateProperty.all(hasElevation ? 2 : 0),
        backgroundColor: MaterialStateProperty.all(buttonColor),
        padding: padding!=null?MaterialStateProperty.all(padding):null,
        shape: !rounded?MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.zero,
          ),
        ):null,
      ),
    );
  }
}
