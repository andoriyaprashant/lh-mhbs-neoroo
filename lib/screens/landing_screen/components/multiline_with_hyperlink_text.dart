import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../../../utils/constants.dart';
import '../../../utils/methods.dart';

class MultiLineWithHyperlinkText extends StatelessWidget {
  const MultiLineWithHyperlinkText({
    super.key,
    required this.deviceWidth,
    required this.tapGestureRecognizer,
    required this.privacyPolicyLink,
    required this.cookiesPolicyLink,
  });
  final TapGestureRecognizer tapGestureRecognizer;
  final double deviceWidth;
  final String privacyPolicyLink;
  final String cookiesPolicyLink;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: convertXdWidthToScreen(xdWidth: 37, deviceWidth: deviceWidth),
      ),
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(text: 'mHBS is powered by DHIS2 to support evidence based intervention in neonates. By continuing, you agree to our ',
                style: TextStyle(color: Colors.black.withOpacity(0.4),
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                )),
            TextSpan(
              text: 'privacy and data use policies',
              style: TextStyle(
                color: hyperlinkTextColor,
                fontSize: 12,
                fontWeight: FontWeight.w700,
                //decoration: TextDecoration.underline,
              ),
              recognizer: tapGestureRecognizer..onTap = () {
                //TODO: Add link to privacy policy
                launchURL2(privacyPolicyLink);
              },
            ),
            TextSpan(text: ', including ',
                style: TextStyle(color: Colors.black.withOpacity(0.4),
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                )),
            TextSpan(
              text: 'cookies',
              style: TextStyle(
                color: hyperlinkTextColor,
                fontSize: 12,
                fontWeight: FontWeight.w700,
                //decoration: TextDecoration.underline,
              ),
              recognizer: tapGestureRecognizer..onTap = () {
                //TODO: Add link to cookies policy
                launchURL2(cookiesPolicyLink);
              },
            ),
            TextSpan(text: ' tracking and usage.',
                style: TextStyle(color: Colors.black.withOpacity(0.4),
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                )),
          ],
        ),
      ),
    );
  }
}
