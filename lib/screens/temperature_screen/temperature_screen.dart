import 'dart:developer';

import 'package:el_tooltip/el_tooltip.dart';
import 'package:flutter/material.dart';
import 'package:neoroo/screens/notifications_screen/notifications_screen.dart';
import 'package:neoroo/screens/shared_widgets/header_of_screen.dart';
import 'package:neoroo/screens/temperature_screen/components/weekly_calendar.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/enums.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent_bottom_nav_bar_v2.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:toggle_switch/toggle_switch.dart';

import 'components/temperature_chart.dart';
import 'package:neoroo/routing/routing.dart' as router;

class TemperatureScreen extends StatefulWidget {
  const TemperatureScreen({Key? key}) : super(key: key);

  @override
  State<TemperatureScreen> createState() => _TemperatureScreenState();
}

class _TemperatureScreenState extends State<TemperatureScreen> {
  late TooltipBehavior  _tooltipBehavior;
  int selected= DateTime.now().day;
  late Unit unit;
  @override
  void initState() {
    // TODO: implement initState
    unit = Unit.Celsius;
    _tooltipBehavior = TooltipBehavior(enable: true,

    );
    selected = DateTime.now().day;
    getCurrentDate();
    super.initState();
  }
  DateTime finalDate = DateTime.now();

  getCurrentDate(){

    final now = DateTime.now();

    var date = DateTime(now.year, now.month+1, 0).toString();

    var dateParse = findFirstDateOfTheWeek(now);

    var formattedDate = "${dateParse.day}/${dateParse.month}/${dateParse.year}";

    setState(() {

      finalDate = dateParse ;

    });

  }

  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday));
  }

  @override
  Widget build(BuildContext context) {

    final List<ChartData> chartData = [

      ChartData(DateTime(2023, 1, 1, 1), 39.6),
      ChartData(DateTime(2023, 1, 1, 1, 30), 38.5),
      ChartData(DateTime(2023, 1, 1, 2), 40),
      ChartData(DateTime(2023, 1, 1, 2, 30), 38),
      ChartData(DateTime(2023, 1, 1, 3), 39),
      ChartData(DateTime(2023, 1, 1, 3, 30), 36.8),
      ChartData(DateTime(2023, 1, 1, 4), 36.9),
      ChartData(DateTime(2023, 1, 1, 4, 30), 36.3),
      ChartData(DateTime(2023, 1, 1, 5), 36),
      ChartData(DateTime(2023, 1, 1, 5, 30), 36.2),
      ChartData(DateTime(2023, 1, 1, 6), 35),
      ChartData(DateTime(2023, 1, 1, 6, 30), 34),
      ChartData(DateTime(2023, 1, 1, 7), 33.5),
      ChartData(DateTime(2023, 1, 1, 7, 30), 34.8),
      ChartData(DateTime(2023, 1, 1, 8), 33),
      ChartData(DateTime(2023, 1, 1, 8, 30), 32),
      ChartData(DateTime(2023, 1, 1, 9), 36.6),
      ChartData(DateTime(2023, 1, 1, 9, 30), 37),
      ChartData(DateTime(2023, 1, 1, 10), 36),
      ChartData(DateTime(2023, 1, 1, 10, 30), 35.8),
      ChartData(DateTime(2023, 1, 1, 11), 37.4),
      ChartData(DateTime(2023, 1, 1, 12), 39.6),
      ChartData(DateTime(2023, 1, 1, 12, 30), 37.6),
      ChartData(DateTime(2023, 1, 1, 13), 39.5),
      ChartData(DateTime(2023, 1, 1, 13, 30), 38),
      ChartData(DateTime(2023, 1, 1, 14), 39),
      ChartData(DateTime(2023, 1, 1, 14, 30), 32.5),
      ChartData(DateTime(2023, 1, 1, 15), 34.5),
      ChartData(DateTime(2023, 1, 1, 15, 30), 35.5),
      ChartData(DateTime(2023, 1, 1, 16), 31.2),
      ChartData(DateTime(2023, 1, 1, 16, 30), 36.5),
      ChartData(DateTime(2023, 1, 1, 17), 37),
    ];
    chartData.sort((a,b)=>a.x.compareTo(b.x));
    double deviceWidth = MediaQuery.of(context).size.width;
    double deviceHeight = MediaQuery.of(context).size.height;
    return  Scaffold(
      backgroundColor: Colors.white,
      appBar: ScreenHeader(
        isUserIconVisible: false,
        title: "Temperature",
        hasBackButton: true,
        backButtonOnPressed: (){
          Navigator.pop(context);
        },
        actionIconImages: [
          "assets/images/notification.png",
          "assets/images/menu.png"
        ],
        actionIconOnPressFunctions: [
              () {
                pushScreen(context, screen: NotificationsScreen(), settings: RouteSettings(name: router.notificationsPage));

              },
          (){
            getCurrentDate.call();
          }
        ],
        hasSearchBar: false,

      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 8,),
            WeeklyCalendar(finalDate: finalDate, onDateChange: (){}),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Align(
                alignment: Alignment.centerRight,
                child: ToggleSwitch(
                  minWidth: (deviceWidth > deviceHeight && deviceWidth > 600)
                    || deviceWidth >960?
                      convertXdHeightToScreen(xdHeight: 47, deviceHeight: deviceWidth, xdScreenHeight: 640)
                      :convertXdWidthToScreen(xdWidth: 47, deviceWidth: deviceWidth, xdScreenWidth: 360),
                  minHeight: 30,
                  cornerRadius: convertXdWidthToScreen(xdWidth: 47, deviceWidth: deviceWidth, xdScreenWidth: 360),
                  activeBgColors: [[primary], [primary]],
                  activeFgColor: Colors.white,
                  centerText: true,
                  animate: true,
                  animationDuration: 300,
                  inactiveBgColor: Color(0xFFE5E5E5),
                  inactiveFgColor: Color(0xFF717171),
                  initialLabelIndex: unit == Unit.Fahrenheit ? 0 : 1,
                  totalSwitches: 2,
                  labels: ['F', 'C°'],
                  radiusStyle: true,
                  onToggle: (index) {

                      if(index == 0){
                        unit = Unit.Fahrenheit;
                      }else{
                        unit = Unit.Celsius;
                      }
                      setState(() {

                      });
                  },
                ),
              ),
            ),
            TemperatureChart(chartData: chartData, unit: unit,),
          ],
        ),
      ),
    );
  }
}


