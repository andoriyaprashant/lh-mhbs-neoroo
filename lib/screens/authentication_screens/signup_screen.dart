import 'package:flutter/material.dart';
import 'package:neoroo/screens/authentication_screens/components/auth_header.dart';
import 'package:neoroo/screens/authentication_screens/components/auth_input_text_field.dart';
import 'package:neoroo/screens/authentication_screens/components/forgot_password.dart';
import 'package:neoroo/screens/authentication_screens/components/remember_me_check_box.dart';
import 'package:neoroo/screens/authentication_screens/components/welcome_widget.dart';
import 'package:neoroo/screens/authentication_screens/login_screen.dart';
import 'package:neoroo/screens/landing_screen/components/main_button.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:neoroo/utils/vertical_space.dart';

import '../../utils/enums.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen(this.userType, {Key? key}) : super(key: key);
  final UserType userType;

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  late TextEditingController _usernameController;
  late TextEditingController _passwordController;
  late TextEditingController _confirmPasswordController;
  bool allFieldsAreFilled = false;
  String userErrorText = "";
  String passwordErrorText = "";
  String confirmPasswordErrorText = "";

  bool rememberMe = false; // TODO(Mehul) : use this to check if the user enabled remember me or not
  @override
  void initState() {
    super.initState();
    _usernameController = TextEditingController();
    _passwordController = TextEditingController();
    _confirmPasswordController = TextEditingController();
  }
  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    super.dispose();
  }

  bool validateSignUpInput(String username, String password, String password2) {

    bool valid = true;
    if (username.isEmpty) {
      setState(() {
        userErrorText = "This field is missing";
      });
      valid = false;
    }else if (!RegExp(r"^[a-zA-Z][a-zA-Z0-9_]*$").hasMatch(username)) {
      showSnackBar(context: context, message: "Username must start with a letter and can only contain letters, numbers, underscores");
      setState(() {
        userErrorText = "Username is not valid";
      });
      valid = false;
    }
    if (password.isEmpty) {
      setState(() {
        passwordErrorText= "This field is missing";
      });
      valid = false;
    }else if (password.length < 8 && widget.userType==UserType.careProvider) {
      setState(() {
        passwordErrorText= "Password must be at least 8 characters";
      });
      valid = false;
    }else if (password.length < 6 && widget.userType==UserType.familyMember) {
      setState(() {
        passwordErrorText= "Password must be at least 6 characters";
      });
      valid = false;
    }
    // Password must contain at least one uppercase letter, one lowercase letter and one number and one special character
    else if   (!RegExp(r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$").hasMatch(
        password) && widget.userType==UserType.careProvider) {
      showSnackBar(context: context, message: "Password must contain at least one uppercase letter, one lowercase letter and one number and one special character");
      setState(() {
        passwordErrorText= "Password is not valid";
      });
      valid = false;
    }
    if (password2.isEmpty) {
      setState(() {
        confirmPasswordErrorText= "This field is missing";
      });
      valid = false;
    }else if (password2 != password && passwordErrorText.length == 0) {
      // showSnackBar(context: context, message: "Passwords do not match");
      setState(() {
        passwordErrorText= "Passwords do not match";
        confirmPasswordErrorText= "Passwords do not match";
      });
    }

    return valid;
  }


  void onChanged(String value){
    if(_usernameController.text.isNotEmpty && _passwordController.text.isNotEmpty && _confirmPasswordController.text.isNotEmpty){
      setState(() {
        allFieldsAreFilled = true;
      });
    }else{
      setState(() {
        allFieldsAreFilled = false;
      });
    }
    setState(() {
      userErrorText = "";
      passwordErrorText = "";
      confirmPasswordErrorText = "";
    });
  }


  @override
  Widget build(BuildContext context) {
    double deviceHeight = MediaQuery.of(context).size.height;
    double deviceWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      //resizeToAvoidBottomInset: false,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              VerticalSpace(height: convertXdHeightToScreen(xdHeight: 34, deviceHeight: deviceHeight)),
              AuthenticationHeader(deviceWidth: deviceWidth, deviceHeight: deviceHeight),
              VerticalSpace(height: convertXdHeightToScreen(xdHeight: 60, deviceHeight: deviceHeight)),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: convertXdWidthToScreen(xdWidth: 46, deviceWidth: deviceWidth)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    WelcomeWidget(deviceWidth: deviceWidth, deviceHeight: deviceHeight, userRole: widget.userType,),
                    VerticalSpace(height: convertXdHeightToScreen(xdHeight: 65, deviceHeight: deviceHeight)),
                    // user name text field
                    AuthInputTextField(placeholder: "", inputType: InputType.username, controller: _usernameController, label: "User Name", isRequired: true, errorText: userErrorText.isEmpty ? null : userErrorText,
                      onChanged: onChanged,
                    ),
                    VerticalSpace(height: convertXdHeightToScreen(xdHeight: 42, deviceHeight: deviceHeight)),
                    // password text field
                    AuthInputTextField(placeholder: "", inputType: InputType.password, controller: _passwordController, label: "Password", isRequired: true, errorText: passwordErrorText.isEmpty ? null : passwordErrorText,
                      onChanged: onChanged,
                    ),
                    VerticalSpace(height: convertXdHeightToScreen(xdHeight: 42, deviceHeight: deviceHeight)),
                    // password text field
                    AuthInputTextField(placeholder: "", inputType: InputType.password, controller: _confirmPasswordController, label: "Confirm Password", isRequired: true, errorText: confirmPasswordErrorText.isEmpty ? null : confirmPasswordErrorText,
                      onChanged: onChanged,
                    ),
                    VerticalSpace(height: convertXdHeightToScreen(xdHeight: 14, deviceHeight: deviceHeight)),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding:  EdgeInsets.only(left: convertXdWidthToScreen(xdWidth: 34, deviceWidth: deviceWidth),
                      right: convertXdWidthToScreen(xdWidth: 46, deviceWidth: deviceWidth)
                  ),
                  child: RememberMeCheckBox(
                    onChecked: (){
                      setState(() {
                        rememberMe = true;
                      });
                    },
                    onUnChecked: (){
                      setState(() {
                        rememberMe = false;
                      });
                    },
                  ),
                ),
              ),
              Padding(
                padding:  EdgeInsets.only(left: convertXdWidthToScreen(xdWidth: 34, deviceWidth: deviceWidth),
                    right: convertXdWidthToScreen(xdWidth: 46, deviceWidth: deviceWidth)
                ),
                child: Column(
                  children: [
                    VerticalSpace(height: convertXdHeightToScreen(xdHeight: 40, deviceHeight: deviceHeight)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        MainButton(deviceWidth: deviceWidth, deviceHeight: deviceHeight,
                          title: "SignIn", onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => LoginScreen(userType: widget.userType)));
                          }, buttonSize: Size(
                              convertXdWidthToScreen(xdWidth: 113, deviceWidth: deviceWidth),
                              convertXdHeightToScreen(xdHeight: 38, deviceHeight: deviceHeight)
                          ),buttonColor: secondary, textColor: Colors.black,
                          textSize: 18,fontWeight: FontWeight.w500,
                        ),
                        const SizedBox(width: 14,),
                        MainButton(deviceWidth: deviceWidth, deviceHeight: deviceHeight,
                          title: "SignUp", onPressed: (){
                            if(validateSignUpInput(_usernameController.text, _passwordController.text, _confirmPasswordController.text)){
                              // TODO (Mehul):: Execute sign up logic here
                            }
                          }, buttonSize: Size(
                              convertXdWidthToScreen(xdWidth: 113, deviceWidth: deviceWidth),
                              convertXdHeightToScreen(xdHeight: 38, deviceHeight: deviceHeight)
                          ),buttonColor:
                          allFieldsAreFilled ? primary : secondary
                          , textColor: allFieldsAreFilled ? Colors.white :Colors.black ,
                          textSize: 18,fontWeight: FontWeight.w500,
                        ),

                      ],
                    )
                  ],
                ),
              )

            ],
          ),
        ),
      ),
    );

  }
}
