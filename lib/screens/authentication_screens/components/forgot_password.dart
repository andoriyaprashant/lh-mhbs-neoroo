import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/methods.dart';

class ForgotPasswordHyperLink extends StatefulWidget {
  /// This widget is used to create a hyperlink to the forgot password page.
  /// It takes [forgotPasswordLink] as the link to the forgot password page.
  const ForgotPasswordHyperLink({Key? key,
  required this.forgotPasswordLink
  }) : super(key: key);
  
  final String forgotPasswordLink;

  @override
  State<ForgotPasswordHyperLink> createState() => _ForgotPasswordHyperLinkState();
}

class _ForgotPasswordHyperLinkState extends State<ForgotPasswordHyperLink> {
  late TapGestureRecognizer tapGestureRecognizer;
  @override
  void initState() {
    super.initState();
    tapGestureRecognizer = TapGestureRecognizer();
  }
  @override
  void dispose() {
    tapGestureRecognizer.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Center(
        child: RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: "Forgot password?",
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: primary,
                  decoration: TextDecoration.underline,

                ),
                recognizer: tapGestureRecognizer..onTap = () {
                  launchURL2(widget.forgotPasswordLink);
                },
              ),
            ],
          )
        )
      ),
    );
  }
}
