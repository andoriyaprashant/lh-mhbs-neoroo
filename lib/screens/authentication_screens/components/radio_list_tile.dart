
import 'package:flutter/material.dart';
import 'package:neoroo/utils/constants.dart';

class RadioListTileCustom extends StatelessWidget {
  const RadioListTileCustom({
    super.key, required this.title, required this.subtitle, required this.value, required this.groupValue, required this.onChanged, required this.onTap,
  });
  final String title;
  final String subtitle;
  final int value;
  final int groupValue;
  final Function(int?) onChanged;
  final VoidCallback onTap;
  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: onTap,
        child: Ink(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment:  MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Radio(value: value, groupValue: groupValue, onChanged: onChanged,
                  activeColor: primary,
                  fillColor: MaterialStateProperty.all(primary),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 4,),
                    Text(title, style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400
                    ),),
                    Text(subtitle, style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Color(0xffA5A4A4)
                    ),),
                  ],
                )


              ],
            ),
          ),
        ),
      ),
    );
  }
}
