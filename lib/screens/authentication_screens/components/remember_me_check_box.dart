import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:neoroo/utils/constants.dart';

class RememberMeCheckBox extends StatefulWidget {
  /// This widget is used to create a checkbox for the remember me option.
  /// It takes [onChecked] as the callback function when the checkbox is checked.
  /// It takes [onUnChecked] as the callback function when the checkbox is unchecked.
  const RememberMeCheckBox({Key? key, this.onChecked, this.onUnChecked}) : super(key: key);
  final VoidCallback? onChecked;
  final VoidCallback? onUnChecked;
  @override
  State<RememberMeCheckBox> createState() => _RememberMeCheckBoxState();
}

class _RememberMeCheckBoxState extends State<RememberMeCheckBox> {
  bool isChecked = false;
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Checkbox(
            value: isChecked,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3),
            ),
            fillColor: isChecked?MaterialStateProperty.all<Color>(primary):
            MaterialStateProperty.all<Color>(Colors.white),

            side: BorderSide(
              color: primary,
              width: 1,
            ),
            onChanged: (value) {
              setState(() {
                isChecked = value!;
              });
              if(isChecked){
                if(widget.onChecked!=null){widget.onChecked!.call();}
              }else{
                if(widget.onUnChecked!=null){widget.onUnChecked!.call();}
              }
            },
          ),
          Text("Remember me",
          style: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w400,
            color: Colors.black,
          ),),
        ],
      )
    );
  }
}
