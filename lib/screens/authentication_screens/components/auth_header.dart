import 'package:flutter/material.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:neoroo/utils/constants.dart';

class AuthenticationHeader extends StatelessWidget {
  /// This widget displays the authentication screen header, including
  /// the logo and a decorative circle image, with additional features like loading
  /// indicator and help icon.
  ///
  /// [deviceWidth] and [deviceHeight] for responsive sizing.
  /// [onLogoTap] is triggered when the logo is tapped.
  /// [isLoading] displays a loading indicator over the decorative circle.
  const AuthenticationHeader({
    Key? key,
    required this.deviceWidth,
    required this.deviceHeight,
    this.onLogoTap,
    this.isLoading = false,
  }) : super(key: key);

  final double deviceWidth;
  final double deviceHeight;
  final VoidCallback? onLogoTap;
  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // Logo with a Hero animation and optional tap functionality
            Padding(
              padding: EdgeInsets.only(
                left: convertXdWidthToScreen(
                  xdWidth: 46,
                  deviceWidth: deviceWidth,
                ),
              ),
              child: GestureDetector(
                onTap: onLogoTap,
                child: Hero(
                  tag: 'app_logo',
                  child: Image.asset(
                    "assets/images/logo-title.jpg",
                    height: convertXdHeightToScreen(
                      xdHeight: 67,
                      deviceHeight: deviceHeight,
                    ),
                    // Display a placeholder if the logo fails to load
                    errorBuilder: (context, error, stackTrace) {
                      return Container(
                        height: convertXdHeightToScreen(
                          xdHeight: 67,
                          deviceHeight: deviceHeight,
                        ),
                        width: 150,
                        color: Colors.grey[200],
                        child: Center(
                          child: Icon(
                            Icons.image_not_supported,
                            color: primary,
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
            
            // Decorative circle image with optional loading overlay
            Stack(
              alignment: Alignment.center,
              children: [
                Image.asset(
                  "assets/images/Ellipse2.png",
                  height: convertXdHeightToScreen(
                    xdHeight: 86,
                    deviceHeight: deviceHeight,
                  ),
                  // Display a placeholder if the circle image fails to load
                  errorBuilder: (context, error, stackTrace) {
                    return Container(
                      height: convertXdHeightToScreen(
                        xdHeight: 86,
                        deviceHeight: deviceHeight,
                      ),
                      width: convertXdHeightToScreen(
                        xdHeight: 86,
                        deviceHeight: deviceHeight,
                      ),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.grey[200],
                      ),
                    );
                  },
                ),
                
                // Loading overlay if isLoading is true
                if (isLoading)
                  Container(
                    height: convertXdHeightToScreen(
                      xdHeight: 86,
                      deviceHeight: deviceHeight,
                    ),
                    width: convertXdHeightToScreen(
                      xdHeight: 86,
                      deviceHeight: deviceHeight,
                    ),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: primary.withOpacity(0.5),
                    ),
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                        strokeWidth: 2,
                      ),
                    ),
                  ),
              ],
            ),
          ],
        ),
        
        // Help icon that shows an authentication help dialog
        Positioned(
          top: 10,
          right: 10,
          child: SafeArea(
            child: IconButton(
              icon: Icon(
                Icons.help_outline,
                color: primary,
              ),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: Text('Authentication Help'),
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Server URL: The DHIS2 server address'),
                        SizedBox(height: 8),
                        Text('Username: Your DHIS2 username'),
                        SizedBox(height: 8),
                        Text('Password: Your DHIS2 password'),
                      ],
                    ),
                    actions: [
                      TextButton(
                        onPressed: () => Navigator.pop(context),
                        child: Text('OK'),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
