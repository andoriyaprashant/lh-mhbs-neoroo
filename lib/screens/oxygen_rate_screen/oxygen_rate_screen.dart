import 'dart:developer';

import 'package:el_tooltip/el_tooltip.dart';
import 'package:flutter/material.dart';
import 'package:neoroo/screens/notifications_screen/notifications_screen.dart';
import 'package:neoroo/screens/shared_widgets/header_of_screen.dart';
import 'package:neoroo/screens/shared_widgets/vital_chart.dart';
import 'package:neoroo/screens/temperature_screen/components/weekly_calendar.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/enums.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent_bottom_nav_bar_v2.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:toggle_switch/toggle_switch.dart';

import '../temperature_screen/components/temperature_chart.dart';
import 'package:neoroo/routing/routing.dart' as router;

class SpO2RateScreen extends StatefulWidget {
  const SpO2RateScreen({Key? key}) : super(key: key);

  @override
  State<SpO2RateScreen> createState() => _SpO2RateScreenState();
}

class _SpO2RateScreenState extends State<SpO2RateScreen> {
  late TooltipBehavior  _tooltipBehavior;
  int selected= DateTime.now().day;
  @override
  void initState() {
    // TODO: implement initState

    _tooltipBehavior = TooltipBehavior(enable: true,

    );
    selected = DateTime.now().day;
    getCurrentDate();
    super.initState();
  }
  DateTime finalDate = DateTime.now();

  getCurrentDate(){

    final now = DateTime.now();

    var date = DateTime(now.year, now.month+1, 0).toString();

    var dateParse = findFirstDateOfTheWeek(now);


    setState(() {

      finalDate = dateParse ;

    });

  }

  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday));
  }

  @override
  Widget build(BuildContext context) {

    final List<ChartData> chartData = [

      ChartData(DateTime(2023, 1, 1, 1), 89.6),
      ChartData(DateTime(2023, 1, 1, 1, 30), 88),
      ChartData(DateTime(2023, 1, 1, 2), 90),
      ChartData(DateTime(2023, 1, 1, 2, 30), 92),
      ChartData(DateTime(2023, 1, 1, 3), 91),
      ChartData(DateTime(2023, 1, 1, 3, 30), 97),
      ChartData(DateTime(2023, 1, 1, 4), 98),
      ChartData(DateTime(2023, 1, 1, 4, 30), 96.5),
      ChartData(DateTime(2023, 1, 1, 5), 95),
      ChartData(DateTime(2023, 1, 1, 5, 30), 93),
      ChartData(DateTime(2023, 1, 1, 6), 92),
      ChartData(DateTime(2023, 1, 1, 6, 30), 94),
      ChartData(DateTime(2023, 1, 1, 7), 93),
      ChartData(DateTime(2023, 1, 1, 7, 30), 99),
      ChartData(DateTime(2023, 1, 1, 8), 98),
      ChartData(DateTime(2023, 1, 1, 8, 30), 99),
      ChartData(DateTime(2023, 1, 1, 9), 97),
      ChartData(DateTime(2023, 1, 1, 9, 30), 100),
      ChartData(DateTime(2023, 1, 1, 10), 98),
      ChartData(DateTime(2023, 1, 1, 10, 30), 97),
      ChartData(DateTime(2023, 1, 1, 11), 99),
      ChartData(DateTime(2023, 1, 1, 12), 96),
      ChartData(DateTime(2023, 1, 1, 12, 30), 94),
      ChartData(DateTime(2023, 1, 1, 13), 91),
      ChartData(DateTime(2023, 1, 1, 13, 30), 88),
      ChartData(DateTime(2023, 1, 1, 14), 92),
      ChartData(DateTime(2023, 1, 1, 14, 30), 91),
      ChartData(DateTime(2023, 1, 1, 15), 93),
      ChartData(DateTime(2023, 1, 1, 15, 30), 94),
      ChartData(DateTime(2023, 1, 1, 16), 97),
      ChartData(DateTime(2023, 1, 1, 16, 30), 98),
      ChartData(DateTime(2023, 1, 1, 17), 100),
    ];
    chartData.sort((a,b)=>a.x.compareTo(b.x));
    double deviceWidth = MediaQuery.of(context).size.width;
    return  Scaffold(
      backgroundColor: Colors.white,
      appBar: ScreenHeader(
        isUserIconVisible: false,
        title: "Blood Oxygen Rate",
        hasBackButton: true,
        backButtonOnPressed: (){
          Navigator.pop(context);
        },
        actionIconImages: [
          "assets/images/notification.png",
          "assets/images/menu.png"
        ],
        actionIconOnPressFunctions: [
              () {
                pushScreen(context, screen: NotificationsScreen(), settings: RouteSettings(name: router.notificationsPage));

              },
              (){

          }
        ],
        hasSearchBar: false,

      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 8,),
            WeeklyCalendar(finalDate: finalDate, onDateChange: (){}),
            VitalChart(chartData: chartData,
              normalBegin: 96,
              normalEnd: 100,
              highBegin: 100,
              highEnd: 100,
              lowBegin: 0,
              lowEnd: 96,
              unit: "%",
              title: "SpO2",
              highAlertImage: "assets/images/oxygen-high.png",
              lowAlertImage: "assets/images/oxygen-high.png",)
          ],
        ),
      ),
    );
  }
}


