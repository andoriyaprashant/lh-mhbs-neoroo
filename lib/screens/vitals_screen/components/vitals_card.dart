import 'package:flutter/material.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/methods.dart';

class VitalTypeCard extends StatefulWidget {
  const VitalTypeCard({
    super.key,
    required this.onTap,
    required this.primaryIcon,
    required this.title,
  });
  final String primaryIcon;
  final VoidCallback onTap;
  final String title;
  @override
  State<VitalTypeCard> createState() => _VitalTypeCardState();
}

class _VitalTypeCardState extends State<VitalTypeCard> {

  Color color = secondaryGrey;

  @override
  Widget build(BuildContext context) {
    double deviceHeight= MediaQuery.of(context).size.height;
    double deviceWidth = MediaQuery.of(context).size.width;
    return ClipRRect(
      borderRadius: BorderRadius.circular(3),
      child: Material(
        child: InkWell(
          borderRadius: BorderRadius.circular(3),
          onTapDown: (TapDownDetails details) {
            setState(() {
              color = primary;
            });
          },
          onTapCancel: () {
            setState(() {
              color = secondaryGrey;
            });
          },
          onTapUp: (TapUpDetails details) {
            setState(() {
              color = secondaryGrey;
            });
          },
          onTap: (){
            widget.onTap.call();
          },
          child: ClipRRect(
            borderRadius: BorderRadius.circular(3),
            child: Ink(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(3),
                color: Colors.white,
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(3),
                child: Ink(

                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                      top: BorderSide(
                          color: color,
                          width: 3.0,
                          style: BorderStyle.solid),
                    ),

                  ),
                  child: Center(
                    child: Padding(
                      padding:  EdgeInsets.symmetric(
                        horizontal: convertXdWidthToScreen(xdWidth: 22, deviceWidth: deviceWidth, xdScreenWidth: 360),
                        vertical: convertXdHeightToScreen(xdHeight: 24, deviceHeight: deviceHeight, xdScreenHeight: 640)
                        ,),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 16.0),
                            child: Text(widget.title,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize:(deviceWidth > deviceHeight && deviceWidth > 600)
                                    || deviceWidth > 960 ?20: 14,
                                fontWeight: FontWeight.w400,
                                color: Color(0xff717171),
                              ),
                            ),
                          ),
                          SizedBox(height: convertXdHeightToScreen(xdHeight: 6, deviceHeight: deviceHeight, xdScreenHeight: 640),),
                          Image.asset(widget.primaryIcon,
                            width: convertXdWidthToScreen(xdWidth: 82, deviceWidth: deviceWidth, xdScreenWidth: 360),),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}