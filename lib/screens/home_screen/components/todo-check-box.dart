import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:neoroo/utils/constants.dart';

class ToDoCheckBox extends StatefulWidget {
  /// This widget is used to create a checkbox for the home_todo section
  /// It takes [onChecked] as the callback function when the checkbox is checked.
  /// It takes [onUnChecked] as the callback function when the checkbox is unchecked.
  /// It takes [title] as the title of the checkbox.
  const ToDoCheckBox({Key? key, this.onChecked, this.onUnChecked, required this.title}) : super(key: key);
  final VoidCallback? onChecked;
  final VoidCallback? onUnChecked;
  final String title;
  @override
  State<ToDoCheckBox> createState() => _ToDoCheckBoxState();
}

class _ToDoCheckBoxState extends State<ToDoCheckBox> {
  bool isChecked = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isChecked = false;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    isChecked = false;
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Material(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Checkbox(
              value: isChecked,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(3),
              ),
              fillColor: isChecked?MaterialStateProperty.all<Color>(primary):
              MaterialStateProperty.all<Color>(Colors.white),

              side: BorderSide(
                color: primary,
                width: 1,
              ),
              onChanged: (value) {
                setState(() {
                  isChecked = value!;
                });
                widget.onChecked!.call();

              },
            ),
            Expanded(
              child: Text(widget.title,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Colors.black,
                ),),
            ),
          ],
        )
    );
  }
}
