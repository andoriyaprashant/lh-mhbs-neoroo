import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:neoroo/screens/home_screen/components/alert_card.dart';
import 'package:neoroo/screens/home_screen/components/todo-check-box.dart';
import 'package:neoroo/screens/landing_screen/components/main_button.dart';
import 'package:neoroo/screens/notifications_screen/notifications_screen.dart';
import 'package:neoroo/screens/shared_widgets/header_of_screen.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/methods.dart';
import 'package:persistent_bottom_nav_bar_v2/persistent_bottom_nav_bar_v2.dart';
import 'package:neoroo/routing/routing.dart' as router;

/// TODO: Enhance the color coding of the alerts
class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.userName, required this.userPictureURL, required this.alerts}) : super(key: key);
  final String userName;
  final String userPictureURL;
  // TODO: replace String with HighPriorityAlert class name (model)
  final List<Map<String, dynamic>> alerts;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<int>deletedAlerts = [];
  int maxAlertsToShow = 3;
  bool isViewMorePressed = false;
  @override
  Widget build(BuildContext context) {
    double deviceWidth= MediaQuery.of(context).size.width;
    double deviceHeight= MediaQuery.of(context).size.height;
    int counter=0;
    return Scaffold(
      key: Key("homePage"),
      appBar: ScreenHeader(
        height: (deviceWidth > deviceHeight && deviceWidth > 600)
            || deviceWidth > 960 ? deviceHeight*0.15 : 80,
        title: "Welcome ${widget.userName}",
        userImage: widget.userPictureURL,
        isUserIconVisible: true,
        actionIconImages: [
          "assets/images/notification.png",
        ],
        actionIconOnPressFunctions: [
          (){
            pushScreen(context, screen: NotificationsScreen(), settings: RouteSettings(name: router.notificationsPage));

          }
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16.0, top: 18, bottom: 4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text("High Priority Alerts",
                  style:TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize:(deviceWidth > deviceHeight && deviceWidth > 600)
                        || deviceWidth > 960 ?16: 14
                  ),
                  ),
                  SizedBox(width: 12,),
                  Image.asset("assets/images/high-alert.png",
                    width:
                    (deviceWidth > deviceHeight && deviceWidth > 600)
                        || deviceWidth > 960 ?null:convertXdWidthToScreen(xdWidth: 22, deviceWidth: deviceWidth, xdScreenWidth: 360),
                    height:
                    (deviceWidth > deviceHeight && deviceWidth > 600)
                        || deviceWidth > 960 ?convertXdHeightToScreen(xdHeight: 28, deviceHeight: deviceHeight, xdScreenHeight: 640):null,),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children:[for (var i = 0; i < widget.alerts.length; i++)

                AnimatedContainer(
                  key: UniqueKey(),
                  height: deletedAlerts.contains(i)?0:null,
                  onEnd: (){
                  },

                  duration: Duration(milliseconds: 300),
                  child: Padding(padding: EdgeInsets.symmetric(vertical: 16, horizontal: 20),
                    child: Material(

                      child: InkWell(
                        onTap: (){
                          print("card is tapped.");
                        },
                        child: Column(
                          key: UniqueKey(),
                          children: [
                            HighPriorityAlertCard(
                              content: widget.alerts[i]["title"],
                              severity: widget.alerts[i]["severity"],
                              type: widget.alerts[i]["type"],
                            ),
                            SizedBox(height: 16,),
                            Row(
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                    child: Container(

                                      decoration: ShapeDecoration(
                                        shape: RoundedRectangleBorder(
                                          side: BorderSide(color: primary, width: 0.5),
                                          borderRadius: BorderRadius.circular(4)
                                        ),


                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(1.0),
                                        child: ToDoCheckBox(
                                          key: UniqueKey(),
                                          title: widget.alerts[i]["todo"],
                                          onChecked: ()async{

                                           // remove the alert from the list
                                            await Future.delayed(Duration(milliseconds: 300));
                                            setState(() {
                                              deletedAlerts.add(i);
                                            });
                                            //
                                          },
                                        ),
                                      )
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),

                      ),
                    ),

                  ),
                )
    ]
            ),

          ],
        ),
      ),

    );
  }
}
