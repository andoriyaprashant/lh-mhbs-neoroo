import 'package:flutter/material.dart';
import 'package:neoroo/screens/notifications_screen/components/notification_card.dart';
import 'package:neoroo/screens/shared_widgets/header_of_screen.dart';
import 'package:neoroo/utils/methods.dart';

class NotificationsScreen extends StatefulWidget {
  const NotificationsScreen({Key? key}) : super(key: key);

  @override
  State<NotificationsScreen> createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {
  TextEditingController searchController = TextEditingController();
  List<Map<String, dynamic>> notificationsList = [
    {
      "title": "New training module added",
      "description":
          "Dr. John has added a new training module for you. Please check it out.",
      "date": '6 JUN ‘20',
      "isRead": false,
    },
    {
      "title": "New training module added",
      "description":
      "Dr. John has added a new training module for you. Please check it out.",
      "date": '6 JUN ‘20',
      "isRead": false,
    },
    {
      "title": "New training module added",
      "description":
      "Dr. John has added a new training module for you. Please check it out.",
      "date": '6 JUN ‘20',
      "isRead": true,
    },
    {
      "title": "New training module added",
      "description":
      "Dr. John has added a new training module for you. Please check it out.",
      "date": '6 JUN ‘20',
      "isRead": false,
    },
    {
      "title": "New training module added",
      "description":
      "Dr. John has added a new training module for you. Please check it out.",
      "date": '6 JUN ‘20',
      "isRead": true,
    },
    {
      "title": "New training module added",
      "description":
      "Dr. John has added a new training module for you. Please check it out.",
      "date": '6 JUN ‘20',
      "isRead": false,
    }
  ];
  @override
  Widget build(BuildContext context) {
    double deviceHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: ScreenHeader(
        isUserIconVisible: false,
        title: "Notifications",
        actionIconImages: [
          "assets/images/notification.png",
        ],
        actionIconOnPressFunctions: [
              () {},
        ],
        hasSearchBar: true,
        searchController: searchController,
        searchHintText: "Search",
        onSearchTextChanged: (text) {
          setState(() {
            // search text
          });
        },
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children:
            notificationsList.map((e) =>
                NotificationCard(
                  title: e['title'],
                  description:
                  e['description'],
                  date: e['date'] ,
                  isRead: e['isRead'] ,
                  onTap: () {},

                )
            ).toList()

          ),
        ),
      ),

    );
  }
}


