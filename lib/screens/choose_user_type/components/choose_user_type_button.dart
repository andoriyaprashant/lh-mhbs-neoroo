import 'package:flutter/material.dart';
import 'package:neoroo/screens/landing_screen/components/main_button.dart';
import 'package:neoroo/utils/constants.dart';
import 'package:neoroo/utils/enums.dart';
import 'package:neoroo/utils/methods.dart';

/// This Button is to choose between different user types on opening the app
/// [userType] is the type of user
/// [onPressed] is the function to be called when the button is pressed
class ChooseUserTypeButton extends StatelessWidget {

  const ChooseUserTypeButton({
    required this.onPressed,
    required this.userType,
    Key? key}) : super(key: key);
  final UserType userType;
  final VoidCallback? onPressed;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return  Center(
      child: SizedBox(
          height: convertXdHeightToScreen(xdHeight: (87-19)+87, deviceHeight: height),
          child: Stack(
            alignment: AlignmentDirectional.center,
            children: [
              Positioned(
                bottom: 0,

                child: MainButton(
                  title: userType==UserType.familyMember? 'Family Member':userType==UserType.careProvider? 'Care Provider':'',
                  buttonSize: Size(275, 87),
                  deviceHeight: height,
                  deviceWidth: width,
                  hasElevation: true,
                  buttonColor: primary,
                  textSize: 18,
                  fontWeight: FontWeight.w500,
                  onPressed: onPressed,
                ),
              ),
              (width > height && width > 600)
                  || width > 960 ?Container(): Positioned(
                top: 0,
                child: Image.asset(
                  userType==UserType.familyMember? "assets/images/family_member_circle_icon.png":"assets/images/care_provider_circle_icon.png",
                  height: convertXdHeightToScreen(xdHeight: 87, deviceHeight: height),),
              )
            ],
          ),
      ),
    );
  }
}
