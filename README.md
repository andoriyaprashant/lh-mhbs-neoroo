# NeoRoo
The NeoRoo app connects the NeoWarm sensor and provides real-time updates to nurses and caregivers about kangaroo mother care metrics.

## To run the app
 - `flutter gen-l10n` - this will generate the localization files
 - `flutter build apk` - will build the Android app 
 - `flutter run` - will run the app

## Documentation
Documentation is maintained in the `pages` branch
 - https://librehealth.gitlab.io/incubating-projects/mhbs/lh-mhbs-neoroo/